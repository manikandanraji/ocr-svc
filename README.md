## OCR Service

Got some pdf and want to find some information without swiping through your pdf viewer? Well, you have come to the right place. Hopefully.

## How to run

- make sure to install docker and docker-compose
- copy `.env.example` file to `.env` and replace placeholder values

```bash
docker compose up --build
```
