from flask import Flask, request, jsonify
import boto3
from flask_cors import CORS
import docx_parser
import utils
from pdf2image import convert_from_path

app = Flask(__name__)
CORS(app)

textract_client = boto3.client("textract")


@app.route("/ping")
def hello_world():
    return "pong\n"


@app.route("/parse-doc", methods=["POST"])
def parse_doc():
    try:
        if "file" not in request.files:
            raise Exception("missing form data: file")

        # save file and determine which parser to use
        f = request.files["file"]
        fileIndex = [
            "application/pdf",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        ].index(f.content_type)
        parser = "pdf" if fileIndex == 0 else "docx"
        file_name = utils.save_file(f)

        response = {}

        if parser == "docx":
            response["docx"] = docx_parser.parse(f)
        else:
            response["pdf"] = []
            images = convert_from_path(file_name)
            for image in images:
                text = utils.extract_text(image)
                table = ""
                if utils.has_table(image):
                    image_bytes = utils.get_image_bytes(image)
                    table = utils.get_table(image_bytes)
                response["pdf"].append({"text": text, "table": table})

        return jsonify({"response": response})
    except Exception as e:
        print(e)
        return jsonify(message="internal server error", error=str(e)), 500
