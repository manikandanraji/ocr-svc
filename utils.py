import os
import cv2
import time
import numpy as np
import pytesseract
import io
from werkzeug.utils import secure_filename
from textractcaller import call_textract, Textract_Features
from textractprettyprinter.t_pretty_print import (
    Pretty_Print_Table_Format,
    Textract_Pretty_Print,
    get_string,
)


def save_file(f):
    file_name = os.path.join(
        "/tmp", str(time.time()) + "_" + secure_filename(f.filename)
    )
    f.save(file_name)
    return file_name


def extract_text(image):
    gray_image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2GRAY)
    text = pytesseract.image_to_string(gray_image, lang="eng", config="--psm 6")
    return text


# extracts the table from image
def has_table(image_arg):
    image = np.array(image_arg)

    # convert the image to grayscale
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # apply gaussian blur to reduce noise and improve contour detection
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)

    # use Canny edge detection to find edges in the image
    edges = cv2.Canny(blurred, 50, 150)

    # find contours in the edged image
    contours, _ = cv2.findContours(edges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    min_table_area = 100000
    filtered_contours = [
        contour for contour in contours if cv2.contourArea(contour) > min_table_area
    ]

    return False if not filtered_contours else True


def get_image_bytes(image):
    image_bytes = io.BytesIO()
    image.save(image_bytes, format="PNG")
    return image_bytes.getvalue()


def get_table(image_bytes):
    response = call_textract(
        input_document=image_bytes, features=[Textract_Features.TABLES]
    )
    return get_string(
        textract_json=response,
        table_format=Pretty_Print_Table_Format.github,
        output_type=[Textract_Pretty_Print.TABLES],
    )
