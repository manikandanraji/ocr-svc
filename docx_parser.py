import docx


def parse(file):
    doc = docx.Document(file)
    lines = []
    for item in doc.iter_inner_content():
        if isinstance(item, docx.text.paragraph.Paragraph):
            lines.append(item.text)
        elif isinstance(item, docx.table.Table):
            table = []
            for row in iter_visual_cells(item):
                cells = [
                    " ".join([cell_item.text for cell_item in iter_block_items(cell)])
                    for cell in row
                ]
                table.append(cells)
            markdown_table = make_markdown_table(table, "left")
            lines.append(markdown_table)
    return "\n".join(lines)


def iter_visual_cells(table):
    visual_cells = []

    for row in table.rows:
        visual_cells.append([])

        for cell in row.cells:
            visual_cells[-1].append(cell)

    return visual_cells


def iter_block_items(blkcntnr):
    for item in blkcntnr.iter_inner_content():
        if isinstance(item, docx.text.paragraph.Paragraph):
            yield item
        elif isinstance(item, docx.table.Table):
            for row in iter_visual_cells(item):
                for cell in row:
                    yield from iter_block_items(cell)


def make_markdown_table(array, align):
    # Find the maximum number of columns in the array
    max_columns = max(len(row) for row in array)

    # Pad shorter sub-arrays with empty strings
    array = [row + [""] * (max_columns - len(row)) for row in array]

    # make sure every elements are strings
    array = [[str(elt) for elt in line] for line in array]

    # get the width of each column
    widths = [max(len(line[i]) for line in array) for i in range(len(array[0]))]

    # make every width at least 3 colmuns, because the separator needs it
    widths = [max(w, 3) for w in widths]

    # center text according to the widths
    array = [[elt.center(w) for elt, w in zip(line, widths)] for line in array]

    # separate the header and the body
    array_head, *array_body = array

    header = "| " + " | ".join(array_head) + " |"

    # alignment of the cells
    align = str(align).lower()  # make sure `align` is a lowercase string
    if align == "none":
        # we are just setting the position of the : in the table.
        # here there are none
        border_left = "| "
        border_center = " | "
        border_right = " |"
    elif align == "center":
        border_left = "|:"
        border_center = ":|:"
        border_right = ":|"
    elif align == "left":
        border_left = "|:"
        border_center = " |:"
        border_right = " |"
    elif align == "right":
        border_left = "| "
        border_center = ":| "
        border_right = ":|"
    else:
        raise ValueError("align must be 'left', 'right' or 'center'.")
    separator = (
        border_left + border_center.join(["-" * w for w in widths]) + border_right
    )

    # body of the table
    body = [""] * len(array)  # empty string list that we fill after
    for idx, line in enumerate(array[1:]):
        # for each line, change the body at the correct index
        body[idx] = "| " + " | ".join(line) + " |"
    body = "\n".join(body)

    return header + "\n" + separator + "\n" + body
