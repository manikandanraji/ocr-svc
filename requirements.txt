amazon_textract_caller==0.2.2
amazon_textract_prettyprinter==0.1.9
boto3==1.34.61
Flask==3.0.2
Flask_Cors==4.0.0
numpy==1.26.4
opencv_python==4.9.0.80
opencv_python_headless==4.9.0.80
pdf2image==1.17.0
pytesseract==0.3.10
python_docx==1.1.0
Werkzeug==3.0.1
gunicorn
